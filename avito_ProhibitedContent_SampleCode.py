﻿# coding: utf-8
"""
Benchmarks for the Avito fraud detection competition
"""
import csv
import re
from collections import defaultdict
import os
import random as rnd
import logging

import nltk.corpus
import scipy.sparse as sp
import numpy as np
from sklearn.linear_model import SGDClassifier
from nltk import SnowballStemmer
from sklearn.externals import joblib


rnd.seed(0)

data_folder = os.path.join('data')

stopwords = frozenset(word.decode('utf-8') for word in nltk.corpus.stopwords.words("russian") if word != "не")
stemmer = SnowballStemmer('russian')
engChars = [ord(char) for char in u"cCyoOBaAKpPeE"]
rusChars = [ord(char) for char in u"сСуоОВаАКрРеЕ"]
eng_rusTranslateTable = dict(zip(engChars, rusChars))
rus_engTranslateTable = dict(zip(rusChars, engChars))

logging.basicConfig(format=u'[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level=logging.NOTSET)


def correct_word(w):
    """ Corrects word by replacing characters with written similarly depending on which language the word. 
        Fraudsters use this technique to avoid detection by anti-fraud algorithms."""

    if len(re.findall(ur"[а-я]", w)) > len(re.findall(ur"[a-z]", w)):
        return w.translate(eng_rusTranslateTable)
    else:
        return w.translate(rus_engTranslateTable)


def get_data_row(file_name, items_limit=None):
    """ Reads data file line by line. """

    with open(file_name) as items_fd:
        sample_indices = set()
        if items_limit:
            logging.info('Items limit set. Extracting ' + str(items_limit) + ' samples')
            try:
                sample_indices = joblib.load(os.path.join(data_folder, "sample_indices.pkl"))
                logging.info('Found sample indices.')
                if len(sample_indices) != items_limit:
                    logging.info('Found ' + str(len(sample_indices)) + ' indices, but required ' + str(items_limit))
                    raise
            except:
                logging.info('Sample indices not found. Starting processing.')
                count_reader = csv.DictReader(items_fd, delimiter='\t', quotechar='"')
                num_items = sum(1 for row in count_reader)
                logging.info('Found ' + str(num_items) + ' items in ' + file_name)
                items_fd.seek(0)
                sample_indices = set(rnd.sample(range(num_items), items_limit))
                joblib.dump(sample_indices, os.path.join(data_folder, "sample_indices.pkl"))
                logging.info('Sample indices dumped')

        logging.info("Reading data...")
        data_row_reader = csv.DictReader(items_fd, delimiter='\t', quotechar='"')

        for i, data_row in enumerate(data_row_reader):
            if not items_limit or i in sample_indices:
                yield {feature_name: feature_value.decode('utf-8') for feature_name, feature_value in
                       data_row.iteritems() if
                       feature_value is not None}


def get_words_from_string(text, stemm_required=False, correct_word_required=False):
    """ Splits the text into words, discards stop words and applies stemmer. 
    Parameters
    ----------
    text : str - initial string
    stemm_required : bool - flag whether stemming required
    correct_word_required : bool - flag whether correction of words required
    """

    clean_text = re.sub(u'[^a-zа-я0-9]', ' ', text.lower())
    if correct_word_required:
        words = [correct_word(w) if not stemm_required or re.search("[0-9a-z]", w)
                 else stemmer.stem(correct_word(w)) for w
                 in clean_text.split() if len(w) > 1 and w not in stopwords]
    else:
        words = [w if not stemm_required or re.search("[0-9a-z]", w) else stemmer.stem(w) for w in clean_text.split() if
                 len(w) > 1 and w not in stopwords]

    return words


def extract_features(file_name, item_limit):
    logging.info('Starting extracting features from ' + str(file_name))
    feature_indexes = {}
    word_counts = defaultdict(lambda: 0)
    for i, item in enumerate(get_data_row(file_name, item_limit)):
        for word in get_words_from_string(item["title"] + " " + item["description"], stemm_required=False,
                                          correct_word_required=False):
            word_counts[word] += 1

        if i % 1000 == 0:
            logging.debug('processed ' + str(i) + ' rows in ' + file_name)

    index = 0
    for word, count in word_counts.iteritems():
        if count >= 3:
            feature_indexes[word] = index
            index += 1

    logging.info(
        'Feature extracting from ' + str(file_name) + 'finished. Extracted ' + str(len(feature_indexes)) + ' features')

    return feature_indexes


def prepare_data(file_name, feature_indices, item_limit=None):
    """ Generate training and testing data. """
    logging.info('Generating feature matrix from ' + file_name)
    targets = []
    item_ids = []
    row = []
    col = []
    cur_row = 0
    for data_row in get_data_row(file_name, item_limit):
        for word in get_words_from_string(data_row["title"] + " " + data_row["description"], stemm_required=False,
                                          correct_word_required=False):
            if word in feature_indices:
                col.append(feature_indices[word])
                row.append(cur_row)

        cur_row += 1
        if "is_blocked" in data_row:
            targets.append(int(data_row["is_blocked"]))
        item_ids.append(int(data_row["itemid"]))

        if cur_row % 1000 == 0:
            logging.debug('processed ' + str(cur_row) + ' rows in ' + file_name)

    features = sp.csr_matrix((np.ones(len(row)), (row, col)), shape=(cur_row, len(feature_indices)),
                             dtype=np.float64)
    logging.info('Feature matrix from ' + file_name + ' constructed.')
    if targets:
        return features, targets, item_ids
    else:
        return features, item_ids


def main():
    """ Generates features and fits classifier. """

    feature_indices = extract_features(os.path.join(data_folder, "avito_train.tsv"), item_limit=300000)
    train_features, train_targets, train_item_ids = prepare_data(os.path.join(data_folder, "avito_train.tsv"),
                                                                 feature_indices,
                                                                 item_limit=300000)
    # joblib.dump((train_features, train_targets, train_item_ids, test_features, test_item_ids),
    # os.path.join(dataFolder, "train_data.pkl"))
    # train_features, train_targets, train_item_ids, test_features, test_item_ids = joblib.load(
    # os.path.join(dataFolder, "train_data.pkl"))
    logging.info("Feature preparation done, fitting model...")
    clf = SGDClassifier(loss="log",
                        penalty="l2",
                        alpha=1e-4,
                        class_weight="auto")
    clf.fit(train_features, train_targets)
    logging.info('Fitting finished.')

    logging.info('Preparing test data.')
    test_features, test_item_ids = prepare_data(os.path.join(data_folder, "avito_test.tsv"), feature_indices)
    logging.info("Predicting...")

    predicted_scores = clf.predict_proba(test_features).T[1]

    logging.info("Write results...")
    output_file = "avito_starter_solution.csv"
    logging.info("Writing submission to %s" % output_file)
    f = open(os.path.join(data_folder, output_file), "w")
    f.write("id\n")

    for pred_score, item_id in sorted(zip(predicted_scores, test_item_ids), reverse=True):
        f.write("%d\n" % item_id)
    f.close()
    logging.info("Done.")


if __name__ == "__main__":
    main()            
